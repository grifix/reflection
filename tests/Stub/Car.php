<?php
declare(strict_types=1);

namespace Grifix\Reflection\Tests\Stub;

final class Car
{
    public function __construct(public string $producer, public Engine $engine)
    {

    }
}
