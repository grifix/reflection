# Installation
`composer install grifix/reflection`

# Usage

```php
final class Car
{
    public function __construct(public string $producer, public Engine $engine)
    {

    }
}

final class Engine
{

    public function __construct(public int $volume)
    {
    }
}

$car = new Car('Mercedes', new Engine(5));
$reflection = new \Grifix\Reflection\ReflectionObject($car);
$reflection->getPropertyValue('producer'); //Mercedes
$reflection->getPropertyValue('engine.volume'); //5
$reflection->setPropertyValue('producer', 'Volvo');
$reflection->setPropertyValue('engine.volume', 10);
$reflection->getObject(); //Car
$reflection->getWrapped(); //\ReflectionObject

```
